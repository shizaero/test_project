#include <iostream>
#include <vector>

//{id, (local_id, group_number)}
using document_type = std::pair<int, std::pair<int, int>>; 

//Вспомогательная функция для вывода одного document_type
void print_element(document_type& elem) {
    std::cout <<elem.first << " " << "(" << elem.second.first <<
        ", " << elem.second.second << ") ";
}

//Вывод вектора с документами
void print_vector(std::string text, 
    std::vector<document_type>& container) {
    std::cout << text;
    for (auto const& elem : container){
        std::cout << "(" << elem.second.first <<
            ", " << elem.second.second << ") ";
    }
    std::cout << std::endl;
}
// Создание структуры с документами
// id был добавлен, как удоб
void get_data(std::vector<document_type>& empty_vector, int size = 10) {
    for (int i = 0; i < size; ++i) {
        empty_vector.push_back({ i, {i + 10, rand() % (12 - 10 + 1) + 10} });
    }
}
//Метод для обработки массива
void process_data(std::vector<document_type>& data, 
    std::vector<document_type>& result, int erase_number) {

    result.push_back(data[0]);//Добавляем первый элемент

    //Цикл с обработкой массива
    for (int i = 1; i < data.size() - 1; ++i) {
        if (data[i].first % erase_number != 0)
            result.push_back(data[i]);
    }
    result.push_back(data.back()); //Добавляем последний элемент

    /*
    * Первая попытка
    std::remove_if(data.begin()+1, data.end()-1,
        [&erase_number](auto& data) {
            print_element(data);
            return data.first % erase_number == 0;
        });
    */
}

int main()
{
    setlocale(LC_ALL, "RU");
    int n = 0;
    std::cin >> n;

    std::string text_begin = "Исходный массив: ";
    std::string test_result = "Обработанный массив: ";

    std::vector<document_type> example_vector;
    std::vector<document_type> result_vector;

    get_data(example_vector);

    print_vector(text_begin, example_vector);

    process_data(example_vector, result_vector, n);

    print_vector(test_result, result_vector);

    system("pause");
    return 0;
}
